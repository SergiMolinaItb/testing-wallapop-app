package cat.itb.wallapopclone.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import cat.itb.wallapopclone.R;

public class AddActivity extends AppCompatActivity {

    MaterialButton detalles;
    TextInputLayout Titulo;
    TextInputEditText titulo;
    TextInputLayout Descripcion;
    TextInputEditText descripcion;
    TextInputLayout Categoria;
    static TextInputEditText categoria;
    TextInputLayout SubCategoria;
    TextInputEditText subcategoria;
    TextInputLayout Estado;
    static TextInputEditText estado;
    TextInputLayout Precio;
    TextInputEditText precio;
    TextInputLayout Moneda;
    AutoCompleteTextView moneda;
    MaterialButton subirProducto;

    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;

    Bundle bundle;

    String t, d, c, s, e, p, m;

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
        t = titulo.getText().toString();
        d = descripcion.getText().toString();
        c = categoria.getText().toString();
        s = subcategoria.getText().toString();
        e = estado.getText().toString();
        p = precio.getText().toString();
        m = moneda.getText().toString();
        save.putString("t", t);
        save.putString("d", d);
        save.putString("c", c);
        save.putString("s", s);
        save.putString("e", e);
        save.putString("p", p);
        save.putString("m", m);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
        t = recover.getString("t");
        d = recover.getString("d");
        c = recover.getString("c");
        s = recover.getString("s");
        e = recover.getString("e");
        p = recover.getString("p");
        m = recover.getString("m");
        titulo.setText(t);
        descripcion.setText(d);
        categoria.setText(c);
        subcategoria.setText(s);
        estado.setText(e);
        precio.setText(p);
        moneda.setText(m);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_activity);
        setTheme(R.style.AppTheme);

        detalles = findViewById(R.id.button_detalles);
        Titulo = findViewById(R.id.titulo_text_inputLayout);
        titulo = findViewById(R.id.tituloText);
        Descripcion = findViewById(R.id.descripcion_text_inputLayout);
        descripcion = findViewById(R.id.descripcionText);
        Categoria = findViewById(R.id.categoria_text_inputLayout);
        categoria = findViewById(R.id.categoriaText);
        SubCategoria = findViewById(R.id.subcategoria_text_inputLayout);
        subcategoria = findViewById(R.id.subcategoriaText);
        Estado = findViewById(R.id.estado_text_inputLayout);
        estado = findViewById(R.id.estadoText);
        Precio = findViewById(R.id.precio_text_inputLayout);
        precio =  findViewById(R.id.precioText);
        Moneda = findViewById(R.id.moneda_text_inputLayout);
        moneda = findViewById(R.id.autoCompleteMoneda);
        subirProducto = findViewById(R.id.button_subir);

        categoria.setInputType(InputType.TYPE_NULL);
        estado.setInputType(InputType.TYPE_NULL);

        arrayList = new ArrayList<>();
        arrayList.add("ARS");
        arrayList.add("MXN");
        arrayList.add("COP");
        arrayList.add("€");
        arrayList.add("USD");
        arrayList.add("GBP");
        arrayList.add("BRL");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item_moneda, arrayList);
        moneda.setAdapter(arrayAdapter);
        moneda.setThreshold(1);

        detalles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(AddActivity.this, MainScreenActivity.class);
                startActivity(d);
            }
        });

        estado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(AddActivity.this, EstadoActivity.class);
                startActivity(e);
            }
        });

        bundle = getIntent().getExtras();
        if(bundle != null) {
            //Categoria
            categoria.setText(bundle.getString("categoria"));
        }

        categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddActivity.this, CategoriesActivity.class);
                startActivity(i);
            }
        });

    }
}
