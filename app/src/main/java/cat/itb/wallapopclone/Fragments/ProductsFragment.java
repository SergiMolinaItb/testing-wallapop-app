package cat.itb.wallapopclone.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.R;

public class ProductsFragment extends Fragment {

    Button button;

    public ProductsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.products_fragment,container,false);

        button =v.findViewById(R.id.button_fovourite_products);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), MainScreenActivity.class);
                startActivity(i);
            }
        });

        return v;
    }
}
