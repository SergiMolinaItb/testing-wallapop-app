package cat.itb.wallapopclone;

import android.view.KeyEvent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.wallapopclone.Activities.AddActivity;
import cat.itb.wallapopclone.Activities.CategoriesActivity;
import cat.itb.wallapopclone.Activities.MainActivity;
import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.Activities.ProductActivity;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressKey;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.espresso.ViewAssertion.*;

@RunWith(AndroidJUnit4.class)
public class ActivityTest {

        public String NAME_TYPED = "Sergi Molina Vera";
        public String EMAIL_TYPED = "123567@1212.cat";
        public String PASS_TYPED = "12345678";
        public String TITLE = "Seat Cupra";
        public String DESCRIPTION = "Coche totalmente nuevo";
        public String PRICE = "15000";

        @Rule
        public ActivityScenarioRule<MainActivity> activityActivityScenarioRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

        @Rule
        public ActivityScenarioRule<MainScreenActivity> activityScreenActivityScenarioRule = new ActivityScenarioRule<MainScreenActivity>(MainScreenActivity.class);

        @Rule
        public ActivityScenarioRule<AddActivity> activityAddActivityScenarioRule = new ActivityScenarioRule<AddActivity>(AddActivity.class);

        @Rule
        public ActivityScenarioRule<CategoriesActivity> activityCategoriesActivityScenarioRule = new ActivityScenarioRule<CategoriesActivity>(CategoriesActivity.class);

        @Rule
        public ActivityScenarioRule<ProductActivity> activityProductActivityScenarioRule = new ActivityScenarioRule<ProductActivity>(ProductActivity.class);

//        @Test
//        public void registerTest() {
//                onView(withId(R.id.registerButton)).perform(click());
//                onView(withId(R.id.nombreText)).perform(typeText(NAME_TYPED)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
//                onView(withId(R.id.emailText)).perform(typeText(EMAIL_TYPED)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
//                onView(withId(R.id.contraseñaText)).perform(typeText(PASS_TYPED)).perform(pressKey(KeyEvent.KEYCODE_ENTER)).perform(ViewActions.closeSoftKeyboard());
//                onView(withId(R.id.iniciarButton)).perform(click());
//                onView(withId(R.id.start_fragment)).check(matches(isDisplayed()));
//        }

        @Test
        public void loginTest() {
                onView(withId(R.id.textLogin2)).perform(click());
                onView(withId(R.id.emailText)).perform(typeText(EMAIL_TYPED)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
                onView(withId(R.id.contraseñaText)).perform(typeText(PASS_TYPED)).perform(pressKey(KeyEvent.KEYCODE_ENTER)).perform(ViewActions.closeSoftKeyboard());
                onView(withId(R.id.iniciarButton)).perform(click());
                onView(withId(R.id.start_fragment)).check(matches(isDisplayed()));
        }

//        @Test
//        public void navigationTest() {
//                loginTest();
//                favouritesTest();
//                uploadTest();
//                messagesTest();
//                profileTest();
//                homeTest();
//        }

        public void homeTest() {
                onView(withId(R.id.startFragment)).perform(click());
                onView(withId(R.id.start_fragment)).check(matches(isDisplayed()));
        }

        public void favouritesTest() {
                onView(withId(R.id.favouritesFragment)).perform(click());
                onView(withId(R.id.favourites_fragment)).check(matches(isDisplayed()));
        }

        public void uploadTest() {
                onView(withId(R.id.addFragment)).perform(click());
                onView(withId(R.id.categories_activity)).check(matches(isDisplayed()));
                onView(withId(R.id.categories_activity)).perform(ViewActions.pressBack());
        }

        public void messagesTest() {
                onView(withId(R.id.messagesFragment)).perform(click());
                onView(withId(R.id.messages_fragment)).check(matches(isDisplayed()));
        }

        public void profileTest() {
                onView(withId(R.id.profileFragment)).perform(click());
                onView(withId(R.id.profile_fragment)).check(matches(isDisplayed()));
        }

        @Test
        public void recyclerView() {
                loginTest();
                onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
                onView(withId(R.id.product_view)).check(matches(isDisplayed()));
        }

        @Test
        public void useCaseTest() {
                loginTest();
                seeProducts();
                createProducts();
                seeMessages();
                closeSession();
        }

        public void seeProducts() {
                onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
                onView(withId(R.id.product_view)).check(matches(isDisplayed()));
                onView(withId(R.id.product_view)).perform(ViewActions.pressBack());
                onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
                onView(withId(R.id.product_view)).check(matches(isDisplayed()));
                onView(withId(R.id.product_view)).perform(ViewActions.pressBack());
        }

        public void createProducts() {
                onView(withId(R.id.addFragment)).perform(click());
                onView(withId(R.id.categories_activity)).check(matches(isDisplayed()));
                onView(withId(R.id.button_coches)).perform(click());
                onView(withId(R.id.tituloText)).perform(typeText(TITLE)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
                onView(withId(R.id.descripcionText)).perform(typeText(DESCRIPTION)).perform(ViewActions.closeSoftKeyboard());
                onView(withId(R.id.estadoText)).perform(click());
                onView(withId(R.id.estadoText)).perform(click());
                onView(withId(R.id.button_sinAbrir)).perform(click());
                onView(withId(R.id.precioText)).perform(typeText(PRICE)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
                onView(withId(R.id.button_subir)).perform(click());
        }


        public void seeMessages() {
                onView(withId(R.id.messagesFragment)).perform(click());
                onView(withId(R.id.messages_fragment)).check(matches(isDisplayed()));
        }

        public void closeSession() {
                onView(withId(R.id.profileFragment)).perform(click());
                onView(withId(R.id._profile_products)).perform(click());
        }
}
