package cat.itb.wallapopclone.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cat.itb.wallapopclone.Adapters.AdapterProducts;
import cat.itb.wallapopclone.Database.ViewModel;
import cat.itb.wallapopclone.R;

public class StartFragment  extends Fragment {

    RecyclerView recyclerView;
    ViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel  =  new ViewModelProvider(getActivity()).get(ViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.start_fragment,container,false);

        recyclerView = v.findViewById(R.id.recycler_main);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        AdapterProducts adapterProducts = new AdapterProducts(ViewModel.getProductos());
        recyclerView.setAdapter(adapterProducts);


        return v;
    }
}
