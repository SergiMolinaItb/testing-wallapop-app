package cat.itb.wallapopclone.Adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.wallapopclone.Activities.ProductActivity;
import cat.itb.wallapopclone.Database.Productos;
import cat.itb.wallapopclone.R;

public class AdapterProducts extends RecyclerView.Adapter<AdapterProducts.ProductViewHolder>{

    List <Productos> productos;

    public AdapterProducts(List<Productos> productos) {
        this.productos = productos;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item,parent,false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.bindData(productos.get(position));
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView precio;
        TextView descripcion;


        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_product_item);
            precio = itemView.findViewById(R.id.textView_product_item_precip);
            descripcion =  itemView.findViewById(R.id.textView_prduct_item_desciption);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), ProductActivity.class);
                    v.getContext().startActivity(i);
                }
            });
        }

        public void bindData(Productos productos){
            precio.setText(String.valueOf((int)productos.getPrecio())+"€");
            descripcion.setText(productos.getDescripción());
            imageView.setImageResource(productos.getImagen());
        }
    }



}
