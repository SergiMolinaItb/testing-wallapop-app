package cat.itb.wallapopclone.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;

import cat.itb.wallapopclone.R;

import static cat.itb.wallapopclone.Activities.AddActivity.categoria;
import static cat.itb.wallapopclone.Activities.AddActivity.estado;

public class EstadoActivity extends AppCompatActivity {

    MaterialButton button_estado;
    MaterialButton sinAbrir;
    MaterialButton nuevo;
    MaterialButton comoNuevo;
    MaterialButton buenEstado;
    MaterialButton aceptable;
    MaterialButton todo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.estado_activity);
        setTheme(R.style.AppTheme);

        button_estado = findViewById(R.id.button_estado);
        sinAbrir = findViewById(R.id.button_sinAbrir);
        nuevo = findViewById(R.id.button_nuevo);
        comoNuevo = findViewById(R.id.button_comoNuevo);
        buenEstado = findViewById(R.id.button_buenEstado);
        aceptable = findViewById(R.id.button_aceptables);
        todo = findViewById(R.id.button_todo);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_estado:
                        onBackPressed();
                        break;
                    case R.id.button_sinAbrir:
                        String sin = "Sin abrir";
                        estado.setText(sin);
                        onBackPressed();
                        break;
                    case R.id.button_nuevo:
                        String nuevo = "Nuevo";
                        estado.setText(nuevo);
                        onBackPressed();
                        break;
                    case R.id.button_comoNuevo:
                        String como = "Como nuevo";
                        estado.setText(como);
                        onBackPressed();
                        break;
                    case R.id.button_buenEstado:
                        String buen = "En buen estado";
                        estado.setText(buen);
                        onBackPressed();
                        break;
                    case R.id.button_aceptables:
                        String acept = "En condiciones aceptables";
                        estado.setText(acept);
                        onBackPressed();
                        break;
                    case R.id.button_todo:
                        String todo = "Lo ha dado todo";
                        estado.setText(todo);
                        onBackPressed();
                        break;
                }
            }
        };

        button_estado.setOnClickListener(listener);
        sinAbrir.setOnClickListener(listener);
        nuevo.setOnClickListener(listener);
        comoNuevo.setOnClickListener(listener);
        buenEstado.setOnClickListener(listener);
        aceptable.setOnClickListener(listener);
        todo.setOnClickListener(listener);
    }
}
