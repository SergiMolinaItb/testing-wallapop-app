package cat.itb.wallapopclone.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import cat.itb.wallapopclone.Adapters.PagerFavouritesAdapter;
import cat.itb.wallapopclone.R;

public class FavouritesFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favourites_fragment,container,false);

        TabLayout tabLayout = v.findViewById(R.id.tabBar_favourites);
        TabItem tabProducts = v.findViewById(R.id.tabItem_products);
        TabItem tabBusquedas = v.findViewById(R.id.tabItem_busquedas);
        TabItem tabPerfiles = v.findViewById(R.id.tabItem_perfiles);
        final ViewPager viewPager = v.findViewById(R.id.viewPagerFavourites);

        PagerAdapter pagerAdapter  = new PagerFavouritesAdapter(getChildFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return v;
    }
}