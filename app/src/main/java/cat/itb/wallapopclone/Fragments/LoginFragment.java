package cat.itb.wallapopclone.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.R;

public class LoginFragment extends Fragment {

    TextInputLayout Email;
    TextInputLayout Contraseña;
    TextInputEditText email;
    TextInputEditText contraseña;
    MaterialButton button_login;
    TextView lost;

    @SuppressLint("ResourceType")
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);
        Email = v.findViewById(R.id.email_text_inputLayout);
        Contraseña = v.findViewById(R.id.contraseña_text_inputLayout);
        email = v.findViewById(R.id.emailText);
        contraseña = v.findViewById(R.id.contraseñaText);
        button_login = v.findViewById(R.id.iniciarButton);
        lost = v.findViewById(R.id.textOlvidado);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.emailText:
                        String verificarEmail = email.getText().toString();
                        if(verificarEmail.isEmpty()){
                            Email.setError("                                                Por favor, Introduce tu email");
                        }else{
                            Email.setError(null);
                        }
                        verificarButton();
                        break;
                    case R.id.contraseñaText:
                        String verificarContraseña = contraseña.getText().toString();
                        if(verificarContraseña.isEmpty()){
                            Contraseña.setError("                                        Por favor, Introduce tu contraseña");
                        }else{
                            Contraseña.setError(null);
                        }
                        verificarButton();
                        break;
                    case R.id.iniciarButton:
                        if(verificarButton()) {
                            Intent i = new Intent(getActivity(), MainScreenActivity.class);
                            i.putExtra("Email", email.getText().toString());
                            startActivity(i);
                        }
                        break;
                    case R.id.textOlvidado:
                        Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_forgotPasswordFragment);
                        break;
                }
            }
        };


        button_login.setOnClickListener(listener);

        email.setOnClickListener(listener);

        contraseña.setOnClickListener(listener);

        lost.setOnClickListener(listener);

        return v;
    }

    public boolean verificarButton(){
        String verificarEmail = email.getText().toString();
        String verificarPassword = contraseña.getText().toString();
        if(!verificarEmail.isEmpty() && !verificarPassword.isEmpty()){
            button_login.setClickable(true);
            return true;
        }else{
            button_login.setClickable(false);
            return false;
        }
    }
}
