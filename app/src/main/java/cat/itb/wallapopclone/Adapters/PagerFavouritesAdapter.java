package cat.itb.wallapopclone.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import cat.itb.wallapopclone.Fragments.BusquedasFragment;
import cat.itb.wallapopclone.Fragments.PerfilesFragment;
import cat.itb.wallapopclone.Fragments.ProductsFragment;

public class PagerFavouritesAdapter extends FragmentPagerAdapter {


    private int numOfTabs;

    public PagerFavouritesAdapter(FragmentManager fm, int  numOfTabs){

        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
            return new ProductsFragment();
            case 1:
                return new BusquedasFragment();
            case 2:
                return new PerfilesFragment();
            default:
                return null;


        }

    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
