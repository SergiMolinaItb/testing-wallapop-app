package cat.itb.wallapopclone.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.slider.Slider;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import cat.itb.wallapopclone.Adapters.SliderAdapter;
import cat.itb.wallapopclone.Fragments.MapsFragment;
import cat.itb.wallapopclone.R;

public class ProductActivity extends AppCompatActivity {
    SliderView sliderView;
    SliderAdapter sliderAdapter;
    int[] images = {R.drawable.producto1item, R.drawable.producto1item2, R.drawable.producto1item3};
    MaterialButton chat;

    Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        sliderView = findViewById(R.id.sliderView);

        sliderAdapter = new SliderAdapter(images);

        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);

        sliderView.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);

        //currentFragment = new MapsFragment();

        //getSupportFragmentManager().beginTransaction().replace(R.id.maps, currentFragment).commit();

        chat = findViewById(R.id.chatButton);

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(ProductActivity.this, MainScreenActivity.class);
                e.putExtra("buzon", "buzon");
                startActivity(e);
            }
        });
    }



}